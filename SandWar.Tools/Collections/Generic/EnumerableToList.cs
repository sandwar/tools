﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SandWar.Tools.Collections.Generic
{
	/// <summary>
	/// Wraps an <see cref="IEnumerable&lt;T&gt;"/> into a readonly <see cref="IList&lt;T&gt;"/>.
	/// </summary>
	public static class EnumerableToListExtensionMethod {
		
		/// <summary>
		/// Wraps an <see cref="IEnumerable&lt;T&gt;"/> into a readonly <see cref="IList&lt;T&gt;"/>.
		/// </summary>
		public static IList<T> AsReadonlyIList<T>(this IEnumerable<T> enumerable) {
			return new EnumerableToList<T>(enumerable);
		}
		
		class EnumerableToList<T> : IList<T>
		{
			IEnumerable<T> enumerable;
			
			public EnumerableToList(IEnumerable<T> enumerable) {
				this.enumerable = enumerable;
			}
			
			public int IndexOf(T item) {
				int i = 0;
				foreach (var v in enumerable) {
					if (Object.Equals(v, item)) return i;
					i++;
				}
				return -1;
			}
			
			public void Insert(int index, T item) {
				throw new NotSupportedException();
			}
			
			public void RemoveAt(int index) {
				throw new NotSupportedException();
			}
			
			public T this[int index] {
				get { return enumerable.ElementAt(index); }
				set { throw new NotSupportedException(); }
			}
			
			public void Add(T item) {
				throw new NotSupportedException();
			}
			
			public void Clear() {
				throw new NotSupportedException();
			}
			
			public bool Contains(T item) {
				return enumerable.Contains(item);
			}
			
			public void CopyTo(T[] array, int arrayIndex) {
				// TODO
				throw new NotImplementedException();
			}
			
			public bool Remove(T item) {
				throw new NotSupportedException();
			}
			
			public int Count {
				get { return enumerable.Count(); }
			}
			
			public bool IsReadOnly {
				get { return true; }
			}
			
			public IEnumerator<T> GetEnumerator() {
				return enumerable.GetEnumerator();
			}
			
			IEnumerator IEnumerable.GetEnumerator() {
				return GetEnumerator();
			}
		}
	}
}
