﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SandWar.Tools.Linq
{
	/// <summary>Provides supplementary Linq-like methods.</summary>
	public static class LinqExtensions
	{
		/// <summary>Excludes an item from a sequence.</summary>
		/// <param name="sequence">A sequence.</param>
		/// <param name="item">The item to be skipped.</param>
		public static IEnumerable<T> Except<T>(this IEnumerable<T> sequence, T item) {
			foreach (var v in sequence)
				if (!Object.Equals(v, item))
					yield return v;
		}
	}
}
