﻿using System;

namespace SandWar.Tools
{
	/// <summary>
	/// Typed weak reference, stores a value but still allows the garbage
	/// collector to destroy it if required.
	/// </summary>
	public struct WeakReference<T>
	{
		readonly WeakReference wr;
		
		WeakReference(T v) {
			wr = new WeakReference(v);
		}
		
		/// <summary>Try to get the weakly referenced value.</summary>
		/// <param name="value">The value, if still present.</param>
		/// <returns>Boolean indication success.</returns>
		public bool TryGetValue(out T value) {
			if (wr == null) {
				value = default(T);
				return false;
			}
			
			var target = wr.Target;
			if (target == null) {
				value = default(T);
				return false;
			}
			
			value = (T)target;
			return true;
		}
		
		public static implicit operator T (WeakReference<T> wr) {
			T value;
			wr.TryGetValue(out value);
			return value;
		}
		
		public static implicit operator WeakReference<T> (T v) {
			return new WeakReference<T>(v);
		}
	}
}
