﻿using System;
using System.Diagnostics;

namespace SandWar.Tools.Debug
{
	/// <summary>Allows to make runtime assertions, automatically removed in release builds.</summary>
	public static class Assert
	{
		/// <summary>Check that a condition is true.</summary>
		/// <param name="condition">The condition to check.</param>
		/// <param name="message">Message to throw if the condition is false.</param>
		[Conditional("DEBUG")]
		public static void That(bool condition, string message) {
			Equal(condition, true, message);
		}
		
		/// <summary>Check that two objects are equal.</summary>
		/// <param name="o1">First object.</param>
		/// <param name="o2">Second object.</param>
		/// <param name="message">Message to throw if the objects are not equal.</param>
		[Conditional("DEBUG")]
		public static void Equal(object o1, object o2, string message) {
			if (!Object.Equals(o1, o2)) throw new AssertionFailedException(message);
		}
		
		/// <summary>Check that two objects are not equal.</summary>
		/// <param name="o1">First object.</param>
		/// <param name="o2">Second object.</param>
		/// <param name="message">Message to throw if the objects are equal.</param>
		[Conditional("DEBUG")]
		public static void NotEqual(object o1, object o2, string message) {
			if (Object.Equals(o1, o2)) throw new AssertionFailedException(message);
		}
	}
}
