﻿using System;
using System.Runtime.Serialization;

namespace SandWar.Tools.Debug
{
	/// <summary>Exception thrown when an assertion is failed.</summary>
	public class AssertionFailedException : Exception
	{
		/// <summary>Initialize a new instance of the <see cref="AssertionFailedException"/> class.</summary>
		public AssertionFailedException() { }
		
		/// <summary>Initializes a new instance of the <see cref="AssertionFailedException"/> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error.</param>
		public AssertionFailedException(string message) : base(message) { }
		
		/// <summary>Initializes a new instance of the <see cref="AssertionFailedException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception, or a null reference if no inner exception is specified.</param>
		public AssertionFailedException(string message, Exception innerException) : base(message, innerException) { }
		
		/// <summary>Initializes a new instance of the Exception class with serialized data.</summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="ArgumentNullException">The <c>info</c> parameter is null.</exception>
		/// <exception cref="SerializationException">The class name is null or <see cref="HResult"/> is zero.</exception>
		protected AssertionFailedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}