﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq;

namespace SandWar.Tools.Reflection
{
	/// <summary>Provides utility reflection extension methods.</summary>
	public static class ExtensionMethods
	{
		/// <summary>Check if a member has an attribute.</summary>
		/// <param name="inherit"><c>true</c> to search this member's inheritance chain to find the attributes; otherwise, <c>false</c>. This parameter is ignored for properties and events.</param>
		// disable once CSharpWarnings::CS1573
		public static bool HasAttribute<T>(this MemberInfo method, bool inherit = false) where T : Attribute {
			return method.GetCustomAttributes(typeof(T), inherit).Any();
		}
		
		/// <summary>Get the implementation of a generic interface <paramref name="interf" />.</summary>
		public static Type GetInterface(this Type type, Type interf) {
			var interfGeneric = interf.IsGenericType ? interf.GetGenericTypeDefinition() : null;
			foreach (var inter in type.GetInterfaces())
				if (inter == interf || inter.IsGenericType && inter.GetGenericTypeDefinition() == interfGeneric)
					return inter;
			
			return null;
		}
		
		/// <summary>Get the method in a class by the method in an interface.</summary>
		public static MethodInfo GetImplementedMethod(this MethodInfo method, Type type) {
			var map = type.GetInterfaceMap(method.DeclaringType);
			for (var i = 0; i < map.InterfaceMethods.Length; i++)
				if (map.InterfaceMethods[i] == method)
					return map.TargetMethods[i];
			
			return null;
		}
		
		/// <summary>Get this <see cref="MethodInfo" /> from the generic <see cref="MethodInfo.ReflectedType" />.</summary>
		public static MethodInfo GetMethodDefinitionFromGeneric(this MethodInfo method) {
			return method.GetMethodDefinitionForType(method.ReflectedType.GetGenericTypeDefinition());
		}
		
		/// <summary>Get this method from another type from the same generic <see cref="MethodInfo.ReflectedType" />.</summary>
		public static MethodInfo GetMethodDefinitionForType(this MethodInfo method, Type type) {
			// TODO: might not work for non-public methods; test
			// TODO: doesn't work for methods with different parameters
			// TODO: doesn't work if a supertype implements another method with the same name and 'new' keyword
			// TODO: might be missing something else important; test more
			return type.GetMethod(method.Name);
		}
		
		/// <summary>Get this <see cref="PropertyInfo" /> from the generic <see cref="PropertyInfo.ReflectedType" />.</summary>
		public static PropertyInfo GetPropertyDefinitionFromGeneric(this PropertyInfo property) {
			return property.GetPropertyDefinitionForType(property.ReflectedType.GetGenericTypeDefinition());
		}
		
		/// <summary>Get this property from another type from the same generic <see cref="PropertyInfo.ReflectedType" />.</summary>
		public static PropertyInfo GetPropertyDefinitionForType(this PropertyInfo property, Type type) {
			// NOTE: might not work for non-public properties
			// NOTE: doesn't distinguish between instance and static properties
			// NOTE: fails for properties of type T, because 'property.PropertyType' doesn't match
			//return type.GetProperty(property.Name, property.PropertyType, property.GetIndexParameters().Select(param => param.ParameterType).ToArray());
			
			// TODO: might not work for non-public properties; test
			// TODO: doesn't work for index accessors with different parameters
			// TODO: doesn't work if a supertype implements another property with the same name and 'new' keyword
			// TODO: might be missing something else important; test more
			return type.GetProperty(property.Name);
		}
	}
}
