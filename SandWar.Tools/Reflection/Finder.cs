﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace SandWar.Tools.Reflection
{
	/// <summary>Provides tools to extract members from types with compile-time checks.</summary>
	public static class Finder
	{
		/// <summary>Used as a reference for types.</summary>
		public static class Type<T> {
		    // disable once StaticFieldInGenericType
		    /// <summary>Dumb value.</summary>
		    public static T V;
		}
		
		/// <summary>Find a method in a type.</summary>
		/// <param name="method"><c>Finder.GetMethod&lt;TextWriter&gt;(writer => writer.Write(default(string)));</c></param>
		public static MethodInfo GetMethod<T>(Expression<Action<T>> method) {
			var methodCallExpression = method.Body as MethodCallExpression;
			if (methodCallExpression == null)
				throw new Exception();  // expects a method call expression
			
			return methodCallExpression.Method;
		}
		
		/// <summary>Find a method in a static type.</summary>
		/// <param name="method"><c>Finder.GetMethod(() => Object.Equals(default(object), default(object));</c></param>
		public static MethodInfo GetMethod(Expression<Action> method) {
			var methodCallExpression = method.Body as MethodCallExpression;
			if (methodCallExpression == null)
				throw new Exception();  // expects a method call expression
			
			return methodCallExpression.Method;
		}
		
		/// <summary>Find a member in a type.</summary>
		/// <param name="member"><c>Finder.GetMember&lt;string&gt;(str => str.Length);</c></param>
		public static MemberInfo GetMember<T>(Expression<Func<T, object>> member) {
			var memberExpression = member.Body as MemberExpression;
			var unaryExpression = member.Body as UnaryExpression;
			if (unaryExpression != null)
				memberExpression = unaryExpression.Operand as MemberExpression;
			if (memberExpression == null)
				throw new Exception();  // expects a member expression
			
			return memberExpression.Member;
		}
		
		/// <summary>Find a member in a static type.</summary>
		/// <param name="member"><c>Finder.GetMember(() => Int32.MaxValue);</c></param>
		public static MemberInfo GetMember(Expression<Func<object>> member) {
			var memberExpression = member.Body as MemberExpression;
			if (memberExpression == null)
				throw new Exception();  // expects a member expression
			
			return memberExpression.Member;
		}
		
		/// <summary>Find a property in a type.</summary>
		/// <param name="property"><c>Finder.GetProperty&lt;string&gt;(str => str.Length);</c></param>
		public static PropertyInfo GetProperty<T>(Expression<Func<T, object>> property) {
			return (PropertyInfo)GetMember<T>(property);
		}
		
		/// <summary>Find a property in a static type.</summary>
		/// <param name="property"><c>Finder.GetProperty(() => Int32.MaxValue);</c></param>
		public static PropertyInfo GetProperty(Expression<Func<object>> property) {
			return (PropertyInfo)GetMember(property);
		}
	}
}
